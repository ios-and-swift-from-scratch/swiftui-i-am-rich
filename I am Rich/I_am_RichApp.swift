//
//  I_am_RichApp.swift
//  I am Rich
//
//  Created by Md. Ebrahim Joy on 13/1/24.
//

import SwiftUI

@main
struct I_am_RichApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
