//
//  ContentView.swift
//  I am Rich
//
//  Created by Md. Ebrahim Joy on 13/1/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
    
        ZStack {
            Color(.systemTeal).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            VStack {
                Text("I Am Rich")
                    .font(.title)
                    .fontWeight(.bold)
                .foregroundColor(Color.white)
            
            }
            Image("diamond")
                .resizable()
                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                .frame(width: 200,height: 200,alignment: .center)
        
        }
        
    
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
